import Vue from 'vue'
import VueRouter from 'vue-router'
import Materials from 'vue-materials'

window.jQuery = window.$ = require('jquery')
require('materialize-css/dist/css/materialize.min.css')
require('materialize-css/dist/js/materialize.min.js')

import App from './App'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import ShowEvent from './pages/event/ShowEvent'
import Calendar from './pages/Calendar'

Vue.use(VueRouter)
Vue.use(Materials)

const routes = [
  { path: '/', component: Login },
  { path: '/register', component: Register },
  { path: '/home', component: Home },
  { path: '/event/:event_id', component: ShowEvent },
  { path: '/app', component: Home },
  { path: '/calendar', component: Calendar }
]

//
// // 3. Create the router instance and pass the `routes` option
// // You can pass in additional options here, but let's
// // keep it simple for now.
const router = new VueRouter({
  routes // short for routes: routes
})

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  template: '<App/>',
  components: { App }
})
